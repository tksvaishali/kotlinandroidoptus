package com.example.androidtest.data.api.services

import com.example.androidtest.data.api.model.response.UserInfo
import retrofit2.Call
import retrofit2.http.GET

interface UserInfoService {
    @GET("users")
    fun getUserInfoData(): Call<List<UserInfo>>
}