package com.example.androidtest.presentation.userinfo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidtest.R
import com.example.androidtest.data.api.model.response.UserInfo
import com.example.androidtest.presentation.model.Error
import com.example.androidtest.presentation.model.Success
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_userinfolist.*
import javax.inject.Inject

class UserInfoListActivity : AppCompatActivity() {
    @Inject
    lateinit var userInfoViewModel: UserInfoListViewModel

    lateinit var userInfoAdapter: UserInfoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_userinfolist)

        initUI()
        observerViewModel()
        callWebservice()
    }

    private fun initUI() {
        userInfoAdapter = UserInfoAdapter()
        rvUserInfoList.apply {
            layoutManager = LinearLayoutManager(this@UserInfoListActivity)
            this.adapter = userInfoAdapter
        }
    }

    private fun callWebservice() {
        userInfoViewModel.getUserInfoListData()
    }

    private fun observerViewModel() {

        userInfoViewModel.userInfoDataState.observe(this, {
            when (it) {
                is Success<*> -> {
                    val userData = it.data as List<UserInfo>
                    userInfoAdapter.listData = userData
                    userInfoAdapter.notifyDataSetChanged()
                }
                is Error -> {
                    Toast.makeText(this, "Error Occurred", Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}