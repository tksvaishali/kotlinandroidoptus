package com.example.androidtest.data.manager

import com.example.androidtest.data.api.CallExecutor
import com.example.androidtest.data.api.model.response.UserInfo
import com.example.androidtest.data.api.services.UserInfoService
import com.example.androidtest.domain.respositories.UserInfoRepository
import io.reactivex.Single
import javax.inject.Inject

class UserInfoManager @Inject constructor(
    private val callExecutor: CallExecutor,
    private val userInfoService: UserInfoService
) : UserInfoRepository {

    override fun getUserInfoData(): Single<List<UserInfo>> {
        return Single.fromCallable {
            val response = callExecutor.execute(userInfoService.getUserInfoData())
            response
        }
    }
}