package com.example.androidtest.data.api

import retrofit2.Call
import javax.inject.Inject

class CallExecutor @Inject constructor(){

    fun <T> execute(call: Call<T>): T? {
        try {
            val response = call.execute()

            if (response.isSuccessful) {
                return response.body()
            } else {
                throw java.lang.Exception()
            }
        } catch (exception: Exception) {
            throw  Exception()
        }
    }
}