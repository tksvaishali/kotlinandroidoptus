package com.example.androidtest.di

import com.example.androidtest.UserInfoApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Component(
    modules = [AndroidInjectionModule::class,
        ActivityModule::class, ViewModelModule::class, RepositoryModule::class,
        NetworkModule::class, ApiModule::class
    ]
)
@Singleton
interface AppComponent {
    fun inject(app: UserInfoApplication)
}