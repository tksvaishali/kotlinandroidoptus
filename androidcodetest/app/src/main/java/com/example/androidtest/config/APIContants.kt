package com.example.androidtest.config

class APIContants {
    companion object {
        const val apiBaseUrl = "https://jsonplaceholder.typicode.com/"
    }
}