package com.example.androidtest.di

import androidx.lifecycle.ViewModel
import com.example.androidtest.presentation.userinfo.UserInfoAdapter
import com.example.androidtest.presentation.userinfo.UserInfoListViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun provideUserInfoViewModel(userInfoListViewModel: UserInfoListViewModel) : ViewModel
}