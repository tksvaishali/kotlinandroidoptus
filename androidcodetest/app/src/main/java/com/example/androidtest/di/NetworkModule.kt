package com.example.androidtest.di

import com.example.androidtest.config.APIContants
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(APIContants.apiBaseUrl)
        .addConverterFactory(
            GsonConverterFactory.create()
        )
        .build()
}