package com.example.androidtest.domain.respositories

import com.example.androidtest.data.api.model.response.UserInfo
import io.reactivex.Single

interface UserInfoRepository {
    fun getUserInfoData(): Single<List<UserInfo>>
}