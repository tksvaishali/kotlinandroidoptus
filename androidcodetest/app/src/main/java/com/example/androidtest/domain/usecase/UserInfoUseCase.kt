package com.example.androidtest.domain.usecase

import com.example.androidtest.data.api.model.response.UserInfo
import com.example.androidtest.domain.respositories.UserInfoRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UserInfoUseCase @Inject constructor(private val userInfoRepository: UserInfoRepository) {
    fun getUserInfoListData(): Single<List<UserInfo>> {
        return userInfoRepository.getUserInfoData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }
}