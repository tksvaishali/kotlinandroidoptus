package com.example.androidtest.presentation.userinfo

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtest.R
import com.example.androidtest.config.inflate
import com.example.androidtest.data.api.model.response.UserInfo
import kotlinx.android.synthetic.main.item_userinfo_listdata_row.view.*
import android.text.style.UnderlineSpan

import android.text.SpannableString




class UserInfoAdapter() : RecyclerView.Adapter<UserInfoAdapter.UserInfoViewHolder>() {
    var listData: List<UserInfo> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserInfoViewHolder {
        val view = parent.inflate(R.layout.item_userinfo_listdata_row, false)
        return UserInfoViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserInfoViewHolder, position: Int) {
        holder.initViews(listData.get(position))
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    class UserInfoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val view: View = itemView
        private val context: Context = itemView.context
        private val txtId: TextView = view.txtID
        private val txtName: TextView = view.txtName
        private val txtEmail: TextView = view.txtEmail
        private val txtPhone: TextView = view.txtPhone

        fun initViews(userInfo: UserInfo) {
            txtId.text = context.getString(R.string.id) + " "+ userInfo.id
            txtName.text = context.getString(R.string.name) + " "+ userInfo.username
            val emailIdValue= context.getString(R.string.email)+ " "+  userInfo.email
            val emailId = SpannableString(emailIdValue)
            emailId.setSpan(UnderlineSpan(), 7, emailIdValue.length, 0)
            txtEmail.text = emailId
            txtPhone.text = context.getString(R.string.phone) + " "+ userInfo.phone
        }
    }
}