package com.example.androidtest.presentation.model

import java.lang.Exception

sealed class ViewState

class Success<T> (val data: T) : ViewState()

object Error : ViewState()

object Loading: ViewState()