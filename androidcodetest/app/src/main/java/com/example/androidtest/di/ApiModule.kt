package com.example.androidtest.di

import com.example.androidtest.data.api.services.UserInfoService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ApiModule {

    @Provides
    fun provideUserInfoApiService(retrofit: Retrofit): UserInfoService =
        retrofit.create(UserInfoService::class.java)
}