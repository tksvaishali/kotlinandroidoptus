package com.example.androidtest.di

import com.example.androidtest.presentation.userinfo.UserInfoListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindUserInfoListActivity() : UserInfoListActivity
}