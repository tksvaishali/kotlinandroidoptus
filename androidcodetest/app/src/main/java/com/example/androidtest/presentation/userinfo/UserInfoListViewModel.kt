package com.example.androidtest.presentation.userinfo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidtest.domain.usecase.UserInfoUseCase
import com.example.androidtest.presentation.model.Error
import com.example.androidtest.presentation.model.Success
import com.example.androidtest.presentation.model.ViewState
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class UserInfoListViewModel @Inject constructor(val userInfoUseCase: UserInfoUseCase) : ViewModel() {
    val userInfoDataState: MutableLiveData<ViewState> = MutableLiveData()
    var disposable: Disposable? = null

    fun getUserInfoListData() {
        disposable = userInfoUseCase.getUserInfoListData()
            .subscribe({
                userInfoDataState.value = Success(it)
            }, {
                userInfoDataState.value = Error
            })
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }
}