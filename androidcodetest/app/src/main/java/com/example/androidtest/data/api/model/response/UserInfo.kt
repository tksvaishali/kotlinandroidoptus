package com.example.androidtest.data.api.model.response

import com.google.gson.annotations.SerializedName

data class UserInfo(
    @SerializedName("id")
    val id: Integer,
    @SerializedName("name")
    val name: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("username")
    val username: String,
    @SerializedName("phone")
    val phone: String
)