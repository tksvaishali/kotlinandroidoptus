package com.example.androidtest.di

import com.example.androidtest.data.manager.UserInfoManager
import com.example.androidtest.domain.respositories.UserInfoRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindUserInfoRepository(userInfoRepository: UserInfoManager): UserInfoRepository
}